import cv2
import datetime

def durationChek(filename):
    data = cv2.VideoCapture(filename)

    frames = data.get(cv2.CAP_PROP_FRAME_COUNT)
    fps = float(data.get(cv2.CAP_PROP_FPS))
    seconds = format(float(frames / fps)*1000, '.0f')
    # video_time = str(datetime.timedelta(milliseconds=seconds)*1000)
    return int(seconds)

# name = "/home/wot-param/Documents/Param/"
# print((durationChek(name)))