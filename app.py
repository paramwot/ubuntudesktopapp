from PyQt5.QtCore import QDir, Qt, QUrl, QSize, QObject, pyqtSignal, QThread
from PyQt5.QtMultimedia import QMediaContent, QMediaPlayer
from PyQt5.QtMultimediaWidgets import QVideoWidget
from PyQt5.QtWidgets import (QApplication, QFileDialog, QHBoxLayout, QLabel,
                             QPushButton, QSizePolicy, QSlider, QStyle, QVBoxLayout, QWidget, QStatusBar, QMainWindow,
                             QDesktopWidget, QStackedLayout)
from PyQt5 import QtCore

from duration import durationChek  #This is to measure duration of video
from responsive import _responsive_width, _responsive_height, responsive, monitor_width, monitor_height #This is to make responsive element
from string_of_main_window import names #for names of buttons and frames
import subprocess
import time
import json
import threading
#config file for file location
with open("config.json","r") as file:
    config = json.load(file)


class SecondWindow(QWidget):
    '''SecondWindow opens after video gets completed'''
    def __init__(self, main_window_obj):
        super().__init__()
        layout = QVBoxLayout()
        self.main_window_obj = main_window_obj
        self.resize(_responsive_width(1280), _responsive_height(800))
        self.setWindowTitle(names["second_window_title"])
        self.pushBtn1 = QPushButton(names["pushBtn1_title"], self)
        self.pushBtn2 = QPushButton(names["pushBtn2_title"], self)
        self.pushBtn1.setGeometry(responsive(monitor_width//2 - 50, monitor_height//2 - 50, 150, 50))
        self.pushBtn2.setGeometry(responsive(monitor_width//2 - 50, monitor_height//2 + 50, 150, 50))
        self.pushBtn1.clicked.connect(self.magic_mirror_unity) #btn to run .sh files
        self.pushBtn2.clicked.connect(self.magic_mirror_emotion) #btn to run .sh files
        self.var_flag = False

        self.second_counter = True  # flag to check mouseDoubleClick to full screen

        self.setWindowFlags(Qt.WindowCloseButtonHint | Qt.WindowMinimizeButtonHint) #to remove maximize icon on screen

        #staylesheet for pushBtn
        style = """
            QPushButton{
            border: 2px solid white;
            border-radius: 8px; 
            color: white;
            background-color:#f4511e; 
            text-align: center; 
            font-size: 16px;
            width: 200px;
            transition: all 0.5s;
            }
            QPushButton:hover{
                    border: 1px #C6C6C6 solid;
                    color: #fff;
                    background: #0892D0;
            }"""

        self.pushBtn1.setStyleSheet(style)
        self.pushBtn2.setStyleSheet(style)
        self.setStyleSheet("background: QLinearGradient( x1:0 y1:0, x2:1 y2:0, stop:0 #000428, stop:1 #004E92);")

        layout.addWidget(self.pushBtn1)
        layout.addWidget(self.pushBtn2)

    def magic_mirror_unity(self):
        self.magic_mirror_unity_var = threading.Thread(target=self.bash1_function)
        self.magic_mirror_unity_var.start()
        # thread2.finished.connect(self.main_window_obj.showFullScreen)
        # thread2.finished.connect(self.main_window_obj.video)

    def magic_mirror_emotion(self):
        self.magic_mirror_emotion_var = threading.Thread(target=self.bash2_function)
        self.magic_mirror_emotion_var.start()

    def bash1_function(self):
        '''function to load .sh file'''
        # subprocess.Popen(config["magic_mirror_unity"])
        self.var_flag = True
        var_process = subprocess.call(['sh', config["magic_mirror_unity"]])
        self.var_flag = False
        self.main_window_obj.showFullScreen()
        self.main_window_obj.video()

    def bash2_function(self):
        '''function to load .sh file'''
        # subprocess.Popen(config["emotion"])
        self.var_flag = True
        subprocess.call(['sh', config["emotion"]])
        self.var_flag = False
        self.main_window_obj.showFullScreen()
        self.main_window_obj.video()

    def mouseDoubleClickEvent(self, event):
        '''overridding method of mouseDoubleClick to full-screen mode'''
        if self.second_counter:
            self.second_counter = False
            self.showMaximized()
        elif self.second_counter==False:
            self.showFullScreen()
            self.second_counter = True


class TimerSecondWindow(QObject):
    '''To measure and call second window after 8sec in QThread'''
    finished = pyqtSignal()
    progress = pyqtSignal(int)

    def run(self):
        start_time = time.time()
        # print("Start time", start_time)
        while True:
            end_time = time.time()
            if end_time - start_time >= config['time_for_second_window']:
                # print("closing thread")
                break
        # print("loop breaked")
        self.finished.emit()


class VideoPlayer(QWidget):
    # Video Player Main widget
    def __init__(self):
        super().__init__()
        self.second_call = SecondWindow(self)
        self.mediaPlayer = QMediaPlayer(None, QMediaPlayer.VideoSurface)
        videoWidget = QVideoWidget()
        self.counter = True #flag to check mouseDoubleClick to full screen
        self.video()
        self.showFullScreen()
        self.thread = QThread()

        self.positionSlider = QSlider(Qt.Horizontal)
        self.positionSlider.setRange(0, 0)
        self.resize(_responsive_width(1280),_responsive_height(800))

        self.magic_mirror_server = threading.Thread(target=self.thread_server)
        self.magic_mirror_server.start()
        # subprocess.call(['sh', config["magic_mirror_server"]])

        layout = QStackedLayout()
        layout.addWidget(videoWidget)
        layout.setCurrentIndex(1)
        self.setLayout(layout)

        self.setWindowFlags(Qt.WindowCloseButtonHint | Qt.WindowMinimizeButtonHint) #to remove maximize icon on screen

        self.mediaPlayer.setVideoOutput(videoWidget)
        self.mediaPlayer.positionChanged.connect(self.positionChanged)
        self.mediaPlayer.durationChanged.connect(self.durationChanged)

    def thread_server(self):
        subprocess.call(['sh', config["magic_mirror_server"]])

    def closeEvent(self, event):
        self.magic_mirror_server.join()

    def mouseDoubleClickEvent(self, event):
        '''overridding method of mouseDoubleClick to full-screen mode'''
        if self.counter:
            self.counter = False
            self.showMaximized()
        elif self.counter == False:
            self.showFullScreen()
            self.counter = True

    def video(self):
        '''to run video'''
        fileName = config['fileName']
        # print(self.second_call.var_flag, "self.second_call.var_flag")
        if fileName != '' and not self.second_call.var_flag:
            self.mediaPlayer.setMedia(
                QMediaContent(QUrl.fromLocalFile(fileName)))
            self.play()
            # print("playing video")
        else:
            self.mediaPlayer.pause()
            self.hide()

    def play(self):
        '''play function according to media state'''
        if self.mediaPlayer.state() == QMediaPlayer.PlayingState:
            self.mediaPlayer.pause()
        else:
            self.mediaPlayer.play()

    def positionChanged(self, position):
        '''function to get compare position with duration of the video'''
        self.positionSlider.setValue(position)
        if durationChek(config["fileName"]) <= position:
            self.hide()
            self.second_call.showFullScreen()
            if self.thread.isRunning():
                self.thread.terminate()
            self.worker = TimerSecondWindow()
            self.worker.moveToThread(self.thread)
            self.thread.started.connect(self.worker.run)
            self.worker.finished.connect(self.thread.quit)
            self.thread.start()
            self.thread.finished.connect(self.second_call.close)
            self.thread.finished.connect(self.showFullScreen)
            self.thread.finished.connect(self.video)
        else:
            None

    def check_for_second_window(self):
        '''function to measure 8 seconds'''
        start_time = time.time()
        # print("Start time", start_time)
        while True:
            end_time = time.time()
            if end_time - start_time >= 8:
                self.second_call.close()
                self.show()
                break

    def durationChanged(self, duration):
        '''functions sets the duration=0 after completion of the video(onece)'''
        self.positionSlider.setRange(0, duration)

    def setPosition(self, position):
        self.mediaPlayer.setPosition(position)

def main():
    import sys
    app = QApplication(sys.argv)
    player = VideoPlayer()
    player.setWindowTitle("ZHL Minerva")
    player.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
